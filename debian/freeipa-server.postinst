#!/bin/sh
set -e

OUT=/dev/null

if [ "$1" = configure ]; then
    if ! getent passwd kdcproxy  > $OUT; then
        adduser --quiet --system --home / \
            --shell /usr/sbin/nologin --group \
            --no-create-home --gecos "IPA KDC Proxy User" \
            kdcproxy > $OUT
    fi
    if ! getent passwd ipaapi  > $OUT; then
        adduser --quiet --system --home / \
            --shell /usr/sbin/nologin --group \
            --no-create-home --gecos "IPA Framework User" \
            ipaapi > $OUT
    fi

    chmod 711 /var/lib/ipa/sysrestore > $OUT || true
    chmod 700 /var/lib/ipa/passwds > $OUT || true
    chmod 700 /var/lib/ipa/private > $OUT || true

    # add www-data to ipaapi group
    if ! id -Gn www-data | grep '\bipaapi\b' >/dev/null; then
        usermod www-data -a -G ipaapi
    fi

    if [ -e /usr/share/apache2/apache2-maintscript-helper ]; then
        . /usr/share/apache2/apache2-maintscript-helper
	if [ ! -e /etc/apache2/mods-enabled/auth_gssapi.load ]; then
            apache2_invoke enmod auth_gssapi || exit $?
        fi
	if [ ! -e /etc/apache2/mods-enabled/authz_user.load ]; then
            apache2_invoke enmod authz_user || exit $?
        fi
	if [ ! -e /etc/apache2/mods-enabled/deflate.load ]; then
            apache2_invoke enmod deflate || exit $?
        fi
	if [ ! -e /etc/apache2/mods-enabled/expires.load ]; then
            apache2_invoke enmod expires || exit $?
        fi
	if [ ! -e /etc/apache2/mods-enabled/headers.load ]; then
            apache2_invoke enmod headers || exit $?
        fi
	if [ ! -e /etc/apache2/mods-enabled/lookup_identity.load ]; then
            apache2_invoke enmod lookup_identity || exit $?
        fi
	if [ ! -e /etc/apache2/mods-enabled/proxy.load ]; then
            apache2_invoke enmod proxy || exit $?
        fi
	if [ ! -e /etc/apache2/mods-enabled/proxy_ajp.load ]; then
            apache2_invoke enmod proxy_ajp || exit $?
        fi
	if [ ! -e /etc/apache2/mods-enabled/proxy_http.load ]; then
            apache2_invoke enmod proxy_http || exit $?
        fi
	if [ ! -e /etc/apache2/mods-enabled/rewrite.load ]; then
            apache2_invoke enmod rewrite || exit $?
        fi
	if [ ! -e /etc/apache2/mods-enabled/session.load ]; then
            apache2_invoke enmod session || exit $?
        fi
	if [ ! -e /etc/apache2/mods-enabled/session_cookie.load ]; then
            apache2_invoke enmod session_cookie || exit $?
        fi
	if [ ! -e /etc/apache2/mods-enabled/ssl.load ]; then
            apache2_invoke enmod ssl || exit $?
        fi

	# Enable default SSL site
	if [ ! -e /etc/apache2/sites-enabled/default-ssl.conf ]; then
            apache2_invoke ensite default-ssl || exit $?
        fi
    fi
fi

#DEBHELPER#
