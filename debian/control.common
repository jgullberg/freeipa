
Package: freeipa-common
Architecture: all
Depends:
 ${misc:Depends},
Multi-Arch: foreign
Description: FreeIPA centralized identity framework -- common files
 FreeIPA is an integrated solution to provide centrally managed Identity
 (machine, user, virtual machines, groups, authentication credentials), Policy
 (configuration settings, access control information) and Audit (events,
 logs, analysis thereof).
 .
 This package includes common files.

Package: freeipa-client
Architecture: any
Depends:
 bind9-utils,
 certmonger (>= 0.79.14),
 curl,
 dnsutils,
 freeipa-common (= ${source:Version}),
 krb5-user,
 libnss3-tools,
 libnss-sss,
 libpam-sss,
 libsasl2-modules-gssapi-mit,
 libsss-sudo,
 oddjob-mkhomedir,
 python3-dnspython,
 python3-ipaclient (= ${source:Version}),
 python3-gssapi,
 python3-ldap,
 python3-sss,
 sssd (>= 2.8.0),
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends}
Recommends:
 chrony,
 sssd-passkey,
Suggests:
 libpam-krb5,
Conflicts:
 systemd-timesyncd,
Description: FreeIPA centralized identity framework -- client
 FreeIPA is an integrated solution to provide centrally managed Identity
 (machine, user, virtual machines, groups, authentication credentials), Policy
 (configuration settings, access control information) and Audit (events,
 logs, analysis thereof).
 .
 This is the client package.

Package: freeipa-client-epn
Architecture: amd64 arm64 armhf i386 mips mips64el mipsel ppc64 ppc64el s390x
Depends:
 freeipa-client (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends}
Breaks: freeipa-server (<< 4.8.10-2)
Replaces: freeipa-server (<< 4.8.10-2)
Description: FreeIPA centralized identity framework -- tools for configuring Expiring Password Notification
 FreeIPA is an integrated solution to provide centrally managed Identity
 (machine, user, virtual machines, groups, authentication credentials), Policy
 (configuration settings, access control information) and Audit (events,
 logs, analysis thereof).
 .
 This package provides a service to collect and send expiring password
 notifications via email (SMTP).

Package: freeipa-client-samba
Architecture: any
Depends:
 cifs-utils,
 freeipa-client (= ${binary:Version}),
 python3-samba,
 samba-common-bin,
 smbclient,
 tdb-tools,
 winbind,
 ${misc:Depends},
 ${python3:Depends},
Multi-Arch: same
Description: FreeIPA centralized identity framework -- Samba client
 FreeIPA is an integrated solution to provide centrally managed Identity
 (machine, user, virtual machines, groups, authentication credentials), Policy
 (configuration settings, access control information) and Audit (events,
 logs, analysis thereof).
 .
 This package provides command-line tools to deploy Samba domain member
 on the machine enrolled into a FreeIPA environment.

Package: python3-ipaclient
Architecture: all
Section: python
Breaks: freeipa-client (<< 4.3.0-1)
Replaces: freeipa-client (<< 4.3.0-1)
Depends:
 freeipa-common (= ${binary:Version}),
 python3-augeas,
 python3-dnspython,
 python3-ipalib (>= ${source:Version}),
 python3-jinja2,
 ${misc:Depends},
 ${python3:Depends},
Description: FreeIPA centralized identity framework -- Python3 modules for ipaclient
 FreeIPA is an integrated solution to provide centrally managed Identity
 (machine, user, virtual machines, groups, authentication credentials), Policy
 (configuration settings, access control information) and Audit (events,
 logs, analysis thereof).
 .
 This Python3 module is used by FreeIPA client.

Package: python3-ipalib
Architecture: all
Section: python
Depends:
 freeipa-common (= ${source:Version}),
 gpg,
 gpg-agent,
 keyutils,
 librpm9,
 python3-cffi,
 python3-cryptography,
 python3-dbus,
 python3-dnspython,
 python3-gssapi,
 python3-ldap,
 python3-libipa-hbac,
 python3-lxml,
 python3-netaddr,
 python3-netifaces (>= 0.10.4),
 python3-pyasn1,
 python3-qrcode (>= 5.0.0),
 python3-requests,
 python3-setuptools,
 python3-six,
 python3-usb (>= 1.0.0~b2),
 python3-yubico,
 systemd,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: FreeIPA centralized identity framework -- shared Python3 modules
 FreeIPA is an integrated solution to provide centrally managed Identity
 (machine, user, virtual machines, groups, authentication credentials), Policy
 (configuration settings, access control information) and Audit (events,
 logs, analysis thereof).
 .
 This Python3 module is used by other FreeIPA packages.

